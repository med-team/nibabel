#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-

BINDIR=$(CURDIR)/debian/tmp/usr/bin/
NIBABEL_MODULE_DIR=$(CURDIR)/debian/*/usr/lib/python*/dist-packages/nibabel

# Setting MPLCONFIGDIR to please matplotlib demanding writable HOME
# on older systems (e.g. 12.04)
export MPLCONFIGDIR=$(CURDIR)/build

%:
	dh $@ --buildsystem=pybuild --builddirectory=build

execute_after_dh_auto_build:
	PYTHONPATH=$(CURDIR) \
	$(MAKE) -C doc html \
		SPHINXBUILD=/usr/share/sphinx/scripts/python3/sphinx-build \
		PYTHON=python3
	# but remove jquery copy (later on link to Debian's version)
	-rm build/html/_static/jquery.js
	# objects inventory is of no use for the package
	-rm build/html/objects.inv
	# also doc source files only consume space
	-rm -r build/html/_sources

# enable when we believe that the tests should pass
override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="PATH={dir}/.pybuild/cpython3_{version}/scripts:$(PATH) PYTHONPATH={build_dir} LC_ALL=C.UTF-8 {interpreter} -m pytest --verbose" \
	dh_auto_test
endif

execute_after_dh_auto_install:
	find $(NIBABEL_MODULE_DIR) -type f -name .gitignore -delete

## immediately useable documentation
## and exemplar data (they are small excerpts anyway)
COMPRESS_EXCLUDE_EXTS := .py .html .css .jpg .txt .js .json .rtc .par .bin
override_dh_compress:
	dh_compress $(foreach EXT,$(COMPRESS_EXCLUDE_EXTS),-X$(EXT))

override_dh_installman:
	PYTHONPATH=build/lib:$(PYTHONPATH) \
	help2man -N $(BINDIR)/parrec2nii \
		-n 'convert PARREC image to NIfTI' \
	> build/parrec2nii.1
	PYTHONPATH=build/lib:$(PYTHONPATH) \
	help2man -N $(BINDIR)/nib-dicomfs \
		-n 'FUSE filesystem on top of a directory with DICOMs' \
	> build/nib-dicomfs.1
	PYTHONPATH=build/lib:$(PYTHONPATH) \
	help2man -N $(BINDIR)/nib-ls \
		-n "'ls' for neuroimaging files" \
	> build/nib-ls.1
	PYTHONPATH=build/lib:$(PYTHONPATH) \
	help2man -N $(BINDIR)/nib-nifti-dx \
		-n "header diagnostic tool" \
	> build/nib-nifti-dx.1
	dh_installman build/*.1

execute_before_dh_fixperms:
	chmod -x $(NIBABEL_MODULE_DIR)/gifti/tests/data/gzipbase64.gii
	chmod -x $(NIBABEL_MODULE_DIR)/tests/data/umass_anonymized.PAR
	chmod -x $(NIBABEL_MODULE_DIR)/nicom/dicomwrappers.py
	chmod -x $(NIBABEL_MODULE_DIR)/nicom/tests/test_dicomwrappers.py

execute_before_dh_clean:
	$(MAKE) clean
